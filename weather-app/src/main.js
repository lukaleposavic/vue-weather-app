import Vue from 'vue'
import App from './App.vue'
import weather from './assets/weather.vue'

Vue.component('weather',weather)

new Vue({
  el: '#app',
  render: h => h(App)
})
